﻿
using System.Collections.Generic;
using Daily.DebugCommand;
using UnityEngine;
using UnityEngine.UIElements;

namespace Daily.DebugConsole
{
    public class DebugConsoleUIToolkit : MonoBehaviour
    {
        public static DebugConsoleUIToolkit Instance { get; private set; }
        public UIDocument UIDocument;
        public PanelSettings _panelSettings;

        private Queue<string> _history = new Queue<string>();
        private List<IDebugCommand> _autoComplete = new List<IDebugCommand>();
        private string _tmpInput;
        private KeyCode switchKey = KeyCode.F12;
        private TextField _txtInput;
        private VisualElement autoCompletion;

        public PanelSettings PanelSettings
        {
            get => _panelSettings;
            private set
            {
                _panelSettings = value;
                UIDocument.panelSettings = _panelSettings;
            }
        }

        public void SetSwitchKey(KeyCode keyCode)
        {
            switchKey = keyCode;
        }

        private void Start()
        {
            UIDocument.rootVisualElement.Add(CreateDebugConsoleUI());
        }

        private void Update()
        {
            CheckSwitchKey();
        }

        private void CheckSwitchKey()
        {
#if ENABLE_LEGACY_INPUT_MANAGER
            if (Input.GetKeyDown(switchKey))
            {
                SwitchOpen();
            }
#endif
        }
        public VisualElement CreateDebugConsoleUI()
        {
            var consoleRoot = CreateRootView();
            var sideBar = CreateSideBar();
            var contextView = new VisualElement();
            contextView.style.width = new StyleLength(StyleKeyword.Auto);
            contextView.style.flexGrow = 1;

            var logView = new VisualElement();
            logView.style.height = new StyleLength(new Length(100, LengthUnit.Percent));

            _txtInput = CreateTextField();
            _txtInput.RegisterCallback<InputEvent>(evt =>
            {
                _tmpInput = evt.newData;
                var autoCompletion = CreateAutoCompletion(_tmpInput);
//                contextView.Add(autoCompletion);
                UIDocument.rootVisualElement.Add(autoCompletion);
            });
            _txtInput.RegisterCallback<KeyDownEvent>(evt =>
            {
                if (evt.keyCode == KeyCode.Return)
                {
                    _history.Enqueue(_tmpInput);
                    DebugCommandExecutor.ExecuteCommand(_tmpInput);
                    _tmpInput = "";
                    _txtInput.value = "";
                    _autoComplete.Clear();
                    autoCompletion.Clear();
                }
                if (evt.keyCode == KeyCode.UpArrow)
                {
                    if (_history.Count > 0)
                    {
                        _txtInput.SetValueWithoutNotify(_history.Dequeue());
                        _tmpInput = _txtInput.value;
                    }
                }
                if (evt.keyCode == KeyCode.DownArrow)
                {
                    if (_autoComplete.Count > 0)
                    {
                        _txtInput.SetValueWithoutNotify(_autoComplete[0].Name);
                        _tmpInput = _autoComplete[0].Name;
                    }
                }
            });


            consoleRoot.Add(sideBar);
            consoleRoot.Add(contextView);
            contextView.Add(logView);
            contextView.Add(_txtInput);
            return consoleRoot;
        }
        private VisualElement CreateAutoCompletion(string input)
        {
            DebugCommandExecutor.GetAutoComplete(input, _autoComplete);

            if (autoCompletion == null)
                autoCompletion = new VisualElement
                {
                    style =
                    {
                        backgroundColor = new Color(0.07f, 0.08f, 0.09f, 0.7f),
                        position = Position.Absolute,
                        borderBottomLeftRadius = 4,
                        borderBottomRightRadius = 4,
                        borderTopLeftRadius = 4,
                        borderTopRightRadius = 4,
                        overflow = Overflow.Visible,
                    }
                };

            autoCompletion.Clear();
            for (int i = 0; i < _autoComplete.Count; ++i)
            {
                string name = _autoComplete[i].Name;
                var label = new Label(name+" "+_autoComplete[i].Info + " " + _autoComplete[i].Format);
                label.style.color = Color.white;
                label.style.fontSize = 24;
                
                label.RegisterCallback<MouseDownEvent>(evt =>
                {
                    _txtInput.SetValueWithoutNotify(name);
                    _tmpInput = name;
                    autoCompletion.RemoveFromHierarchy();
                    autoCompletion = null;
                });
                autoCompletion.Add(label);
            }


            autoCompletion.style.top = _txtInput.layout.yMax;
            autoCompletion.style.left = _txtInput.worldBound.xMin;
            return autoCompletion;
        }
        private void SwitchOpen()
        {
            UIDocument.rootVisualElement.visible = !UIDocument.rootVisualElement.visible;
        }

        private static TextField CreateTextField()
        {
            var txtInput = new TextField();
            txtInput.style.color = Color.white;
            txtInput.style.fontSize = 24;
            txtInput.style.marginLeft = 0;
            txtInput.style.marginRight = 0;
            txtInput.textSelection.cursorColor = Color.white;
            txtInput.textSelection.selectionColor = Color.white;

            var textInput = txtInput.Q<VisualElement>("unity-text-input");
            textInput.style.width = new StyleLength(new Length(100, LengthUnit.Percent));
            textInput.style.height = new StyleLength(new Length(40, LengthUnit.Pixel));
            textInput.style.marginLeft = 0;
            textInput.style.borderLeftWidth = 0;
            textInput.style.borderRightWidth = 0;
            textInput.style.borderTopWidth = 0;
            textInput.style.borderBottomWidth = 0;
            textInput.style.backgroundColor = new Color(0.6f, 0.6f, 0.6f, 0.7f);
            return txtInput;
        }

        private VisualElement CreateSideBar()
        {
            var sideBar = new VisualElement();
            sideBar.style.width = new StyleLength(new Length(10, LengthUnit.Percent));
            sideBar.style.height = new StyleLength(new Length(100, LengthUnit.Percent));
            sideBar.style.backgroundColor = new Color(0.07f, 0.08f, 0.09f, 0.7f);
            sideBar.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);
            sideBar.style.alignItems = new StyleEnum<Align>(Align.FlexStart);
            sideBar.style.justifyContent = new StyleEnum<Justify>(Justify.Center);
            sideBar.style.paddingTop = 10;

            var closeButton = new Button(SwitchOpen);
            closeButton.text = "X";
            closeButton.style.color = Color.white;
            closeButton.style.width = new StyleLength(new Length(40, LengthUnit.Pixel));
            closeButton.style.height = new StyleLength(new Length(40, LengthUnit.Pixel));
            closeButton.style.backgroundColor = new Color(0.9f, 0.4f, 0.4f, 1f);
            closeButton.style.borderBottomLeftRadius = 10;
            closeButton.style.borderBottomRightRadius = 10;
            closeButton.style.borderTopLeftRadius = 10;
            closeButton.style.borderTopRightRadius = 10;
            closeButton.focusable = false;
            
            sideBar.Add(closeButton);

            return sideBar;
        }
        private static VisualElement CreateRootView()
        {
            var consoleRoot = new VisualElement();
            consoleRoot.style.width = new StyleLength(new Length(50, LengthUnit.Percent));
            consoleRoot.style.height = new StyleLength(new Length(30, LengthUnit.Percent));
            consoleRoot.style.backgroundColor = new Color(0.07f, 0.08f, 0.09f, 0.7f);
            consoleRoot.style.borderBottomLeftRadius = 10;
            consoleRoot.style.borderBottomRightRadius = 10;
            consoleRoot.style.borderTopLeftRadius = 10;
            consoleRoot.style.borderTopRightRadius = 10;
            consoleRoot.style.flexDirection = new StyleEnum<FlexDirection>(FlexDirection.Row);
            consoleRoot.style.overflow = Overflow.Hidden;
            return consoleRoot;
        }


        public static void Create(PanelSettings panelSettings)
        {
            var obj = new GameObject("DailyDebugConsoleUIToolkit");
            var ui = obj.AddComponent<DebugConsoleUIToolkit>();
            ui.UIDocument = obj.AddComponent<UIDocument>();
            ui.PanelSettings = panelSettings;

            DontDestroyOnLoad(obj);
            Instance = ui;
        }
    }
}