﻿using System.Collections.Generic;
using Daily.DebugCommand;
using UnityEngine;

namespace Daily.DebugConsole
{
    public class DebugConsoleUI : MonoBehaviour
    {
        private List<IDebugCommand> _autoComplete = new List<IDebugCommand>();
        private List<string> _tmpCommand = new List<string>();
        private int _commandIndex = -1;
        private bool _showUI = false;
        private string _inputText = "";

        public string InputText
        {
            get => _inputText;
            set
            {
                if (!string.IsNullOrEmpty(value) && _inputText == value)
                {
                    DebugCommandExecutor.GetAutoComplete(value, _autoComplete);
                }
                
                if (string.IsNullOrEmpty(_inputText))
                {
                    _autoComplete.Clear();
                }

                _inputText = value;
            }
        }

        public void SwitchOpen()
        {
            _showUI = !_showUI;
        }

        private void OnGUI()
        {
            if (!_showUI) return;
            var screenScale = Screen.width / 720f;
            var scaledMatrix = Matrix4x4.Scale(new Vector3(screenScale, screenScale, screenScale));
            GUI.matrix = scaledMatrix;

            GUI.Box(new Rect(0, 0, 300, 20), "");

            var tmpColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.clear;
            InputText = GUI.TextField(new Rect(0, 0, 300, 20), InputText);

            GUI.backgroundColor = tmpColor;
            GUI.skin.button.normal.background = null;
            for (var index = 0; index < _autoComplete.Count; index++)
            {
                var item = _autoComplete[index];
                if (GUI.Button(new Rect(0, 20 + (20 * index), 200, 20), $"{item.Name} {item.Info}"))
                {
                    InputText = item.Name;
                }
            }

            if (Event.current.keyCode == KeyCode.Return)
            {
                if (string.IsNullOrEmpty(InputText)) return;
                DebugCommandExecutor.ExecuteCommand(InputText);
                _tmpCommand.Insert(0, InputText);
                InputText = "";
                _commandIndex = -1;
            }
            if (Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.UpArrow)
            {
                GetLastCommand();
            }
            if (Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.DownArrow)
            {
                GetNextCommand();
            }
        }
        private void GetLastCommand()
        {
            if (_tmpCommand.Count <= 0) return;

            _commandIndex++;
            _commandIndex = Mathf.Clamp(_commandIndex, 0, _tmpCommand.Count - 1);
            InputText = _tmpCommand[_commandIndex];
        }
        private void GetNextCommand()
        {
            if (_tmpCommand.Count <= 0) return;

            _commandIndex--;
            _commandIndex = Mathf.Clamp(_commandIndex, 0, _tmpCommand.Count - 1);
            InputText = _tmpCommand[_commandIndex];
        }
    }
}