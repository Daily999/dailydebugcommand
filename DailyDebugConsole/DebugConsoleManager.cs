﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Daily.DebugConsole
{
    public static class DebugConsoleManager
    {
        public static Action LogUpdateEvent = delegate { };
        public static Action LogClear = delegate { };
        
        private static List<LogData> _logData = new List<LogData>(500);
        public static int InfoCount { get; private set; }
        public static int ErrorCount { get; private set; }
        public static int WaringCount { get; private set; }
        
        public static int NowLogCount => _logData.Count;
        
        public static void Init()
        {
            Application.logMessageReceivedThreaded += HandleLog;
        }
        
        public static void StopLog()
        {
            Application.logMessageReceivedThreaded -= HandleLog;
        }

        public static void ReleaseData()
        {
            WaringCount = 0;
            ErrorCount = 0;
            InfoCount = 0;
            _logData.Clear();
            LogClear?.Invoke();
        }
        
        public static LogData GetLogData(int index)
        {
            return _logData[index];
        }
        
        private static void HandleLog(string condition, string stacktrace, LogType type)
        {
            _logData.Add(new LogData()
            {
                Condition = condition,
                Stacktrace = stacktrace,
                Type = type,
            });

            switch (type)
            {
                case LogType.Exception:
                case LogType.Assert:
                case LogType.Error:
                    ErrorCount++;
                    break;
                case LogType.Warning:
                    WaringCount++;
                    break;
                case LogType.Log:
                    InfoCount++;
                    break;
                
            }
            LogUpdateEvent.Invoke();
        }
    }
}