﻿using UnityEngine;

namespace Daily.DebugConsole
{
    public static class DailyDebugConsoleHelper
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        public static void InitDebugConsole()
        {
            DebugConsoleManager.Init();
        }

        private static DebugConsoleUI _tmpUI;
        
        #if UNITY_EDITOR
        [UnityEditor.MenuItem("DailyTools/Debug/DebugConsole _`")]
        #endif
        public static void SwitchUI()
        {
            if (!Application.isPlaying)
                return;

            if (_tmpUI != null)
            {
                _tmpUI.SwitchOpen();
                return;
            }

            var obj = new GameObject("DailyDebugCommandIMGUI");
            _tmpUI = obj.AddComponent<DebugConsoleUI>();
            _tmpUI.SwitchOpen();
        }
    }
}