﻿using UnityEngine;
namespace Daily.DebugConsole
{
    public struct LogData
    {
        public string Condition;
        public string Stacktrace;
        public LogType Type;
    }
}