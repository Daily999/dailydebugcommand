﻿using System;

namespace Daily.DebugCommand
{
    public class DebugCommand : IDebugCommand
    {
        private Action<DebugCommandParameter> _command = delegate { };
        public int NameHash { get; private set; }
        public string Name { get; private set; }
        public string Info { get; private set; }
        public string Format { get; private set; }

        public DebugCommand(string name, string info, string format, Action<DebugCommandParameter> command)
        {
            Name = name;
            Info = info;
            Format = format;
            NameHash = name.ToUpper().GetHashCode();
            _command = command;
        }

        public void Execute(string[] parameters) => _command.Invoke(new DebugCommandParameter(parameters));
    }
}