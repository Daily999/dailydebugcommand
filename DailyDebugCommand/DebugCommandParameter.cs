﻿
namespace Daily.DebugCommand
{
    public class DebugCommandParameter
    {
        private string[] _rowParameter;
        
        public DebugCommandParameter(string[] parameter) => _rowParameter = parameter;

        public string GetString(int index)
        {
            if (_rowParameter.Length <= index)
            {
                UnityEngine.Debug.LogWarning($"[DebugCommand] 參數不足");
                return null;
            }
            return _rowParameter[index];
        }
        
        public int GetInt(int index)
        {
            if (_rowParameter.Length <= index)
            {
                UnityEngine.Debug.LogWarning($"[DebugCommand] 參數不足");
                return 0;
            }

            if (int.TryParse(_rowParameter[index], out var var))
            {
                return var;
            }

            UnityEngine.Debug.LogWarning($"[DebugCommand] 無法將{_rowParameter[index]}解析為 int");
            return 0;
        }
        
        public float GetFloat(int index)
        {
            if (_rowParameter.Length <= index)
            {
                UnityEngine.Debug.LogWarning($"[DebugCommand] 參數不足");
                return 0;
            }

            if (float.TryParse(_rowParameter[index], out var var))
            {
                return var;
            }

            UnityEngine.Debug.LogWarning($"[DebugCommand] 無法將{_rowParameter[index]}解析為 float");
            return 0;
        }
    }
}