﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Daily.DebugCommand
{
    [AttributeUsage(AttributeTargets.Method)]
    public class DCommandAttribute : Attribute
    {
        public string Name { get; private set; }
        public string Info { get; private set; }
        public DCommandAttribute(string name, string info)
        {
            Name = name;
            Info = info;
        }
    }

    public static class CommandCreator
    {
        #if UNITY_EDITOR
        [RuntimeInitializeOnLoadMethod]
        public static void CreateCommandInEditor()
        {
            var methods = TypeCache.GetMethodsWithAttribute<DCommandAttribute>();
            foreach (var method in methods)
            {
                var attribute = method.GetCustomAttribute<DCommandAttribute>();
                var format = "";
                foreach (var para in method.GetParameters())
                {
                    format += para.ParameterType + " " + para.Name + ", ";
                }

                DebugCommandExecutor.AddCommand(attribute.Name, attribute.Info, format, data =>
                {
                    var parameters = method.GetParameters();
                    var commandPar = new object[parameters.Length];
                    foreach (var p in parameters)
                    {
                        if (p.ParameterType == typeof(string))
                        {
                            commandPar[p.Position] = data.GetString(p.Position);
                        }
                        else if (p.ParameterType == typeof(int))
                        {
                            commandPar[p.Position] = data.GetInt(p.Position);
                        }
                        else if (p.ParameterType == typeof(float))
                        {
                            commandPar[p.Position] = data.GetFloat(p.Position);
                        }
                        else if (p.ParameterType == typeof(bool))
                        {
                            commandPar[p.Position] = data.GetString(p.Position).ToLower().Equals("true");
                        }
                        else
                        {
                            Debug.LogError($"[DebugCommand] {p.ParameterType} is not support");
                        }
                    }
                    method.Invoke(null, commandPar);
                });
            }
        }
        #endif
        
        public static void CreateCommandInRuntime()
        {
            var types = GetAllGameServiceTypes();

            foreach (var method in types)
            {
                var attribute = method.GetCustomAttribute<DCommandAttribute>();
                var format = "";
                foreach (var para in method.GetParameters())
                {
                    format += para.ParameterType + " " + para.Name + ", ";
                }

                DebugCommandExecutor.AddCommand(attribute.Name, attribute.Info, format, data =>
                {
                    var parameters = method.GetParameters();
                    var commandPar = new object[parameters.Length];
                    foreach (var p in parameters)
                    {
                        if (p.ParameterType == typeof(string))
                        {
                            commandPar[p.Position] = data.GetString(p.Position);
                        }
                        else if (p.ParameterType == typeof(int))
                        {
                            commandPar[p.Position] = data.GetInt(p.Position);
                        }
                        else if (p.ParameterType == typeof(float))
                        {
                            commandPar[p.Position] = data.GetFloat(p.Position);
                        }
                        else if (p.ParameterType == typeof(bool))
                        {
                            commandPar[p.Position] = data.GetString(p.Position).ToLower().Equals("true");
                        }
                        else
                        {
                            Debug.LogError($"[DebugCommand] {p.ParameterType} is not support");
                        }
                    }

                    method.Invoke(null, commandPar);
                });
            }
        }
        public static IEnumerable<MethodInfo> GetAllGameServiceTypes()
        {
            var type = typeof(DCommandAttribute);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var ret = assemblies
                .SelectMany(s => s.GetTypes())
                .SelectMany(t => t.GetMethods())
                .Where(m => m.GetCustomAttribute(type, true) != null);
            return ret;
        }

    }
    public static class DebugCommandExecutor
    {
        private static Dictionary<int, IDebugCommand> _commands;
        private const int MaxAutoComplete = 6;

        static DebugCommandExecutor()
        {
            _commands = new Dictionary<int, IDebugCommand>();
            AddCommand("help", "顯示幫助", "help", _ =>
            {
                foreach (var item in _commands.Values)
                {
                    Debug.Log($"{item.Name} {item.Info} {item.Format}");
                }
            });

        }

        public static void AddCommand(string name,
            string info,
            string format,
            Action<DebugCommandParameter> command)
        {
            RegisterCommand(new DebugCommand(name, info, format, command));
        }

        public static void RegisterCommand(IDebugCommand newCmd) => _commands.TryAdd(newCmd.NameHash, newCmd);

        public static void ExecuteCommand(string cmd)
        {
            if (string.IsNullOrEmpty(cmd))
                return;

            var separators = new[] { ' ', '.' };
            var subs = cmd.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            if (subs.Length <= 0)
                return;

            var commandNameHash = subs[0].ToUpper().GetHashCode();

            var commandParameters = new string[subs.Length - 1];
            for (int i = 0; i < subs.Length - 1; i++)
            {
                commandParameters[i] = subs[i + 1];
            }

            if (!_commands.TryGetValue(commandNameHash, out var command))
            {
                Debug.LogWarning($"找不到命令\"{subs[0]}\"");
                return;
            }

            command.Execute(commandParameters);
        }

        public static void GetAutoComplete(string text, List<IDebugCommand> list)
        {
            list.Clear();
            if (string.IsNullOrEmpty(text))
                return;

            text = text.ToUpper();

            foreach (var command in _commands.Values)
            {
                if (command.Name.ToUpper().Contains(text))
                {
                    list.Add(command);
                }

                if (list.Count >= MaxAutoComplete)
                {
                    return;
                }
            }
        }

        public static bool Verify(bool condition, string message)
        {
            if (!condition)
            {
                Debug.LogError($"[DebugCommand] {message}");
            }

            return condition;
        }
    }
}