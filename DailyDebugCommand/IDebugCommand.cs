﻿namespace Daily.DebugCommand
{
    public interface IDebugCommand
    {
        int NameHash { get; }
        string Name { get; }
        string Info { get; }
        string Format { get; }

        void Execute(string[] parameters);
    }

}